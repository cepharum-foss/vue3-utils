import {ComputedRef, Ref, UnwrapNestedRefs} from "vue";

declare namespace Cepharum.Vue3Utils {
    /**
     * Provides methods for interacting with a single record of cache.
     */
    export class CacheRecord<T> {
        /**
         * Detects if cache record exists and is still valid.
         */
        exists(): boolean;

        /**
         * Fetches content of existing cache record.
         *
         * @note Reading a cache record isn't checking its validity by means of
         *       checking its age to prevent race conditions in accessing it.
         *       Use exists() to make sure cache record exists and is still
         *       valid.
         *
         * @param refresh if true (default), the record's time to live is reset on reading
         */
        read( refresh?: boolean ): T;

        /**
         * Creates/updates record in cache to represent provided value.
         *
         * @param value desired content of cache record
         * @returns provided value
         */
        write(value: T): T;

        /**
         * Explicitly drops an existing cache record.
         */
        delete(): CacheRecord<T>;

        /**
         * Checks all existing records of cache and drops all outdated ones to
         * free memory.
         *
         * @returns numbers of existing cache records before and after running the clean-up
         */
        static gc(): { before: number, after: number };

        /**
         * Enables garbage collection of outdated cache records every given
		 * number of seconds.
		 *
		 * Garbage collection is enabled by default.
         *
         * @param cycleSeconds number of seconds to wait before re-running another garbage collection
         */
        static enableGC( cycleSeconds: number ): void;

        /**
         * Disables automatic garbage collection of outdated cache records.
         */
        static disableGC(): void;
    }

    /**
     * Gains access on a single record of cache.
     *
     * @param key uniquely identifies a cache record in context of its namespace
     * @param namespace namespace of a selected cache record, defaults to empty string
     * @param maxAge maximum age of existing cache records in seconds to be considered on checking
     * @returns API for accessing selected record of cache
     */
    export function useAgingCache<T>(key: string, namespace?: string, maxAge?: number): CacheRecord<T>;

    /**
     * Provides reactive result of an awaitable.
     */
    export interface ReactiveAwaitable<T> {
        /**
         * Provides awaited value when available.
         */
        record?: T;

        /**
         * Provides error caught instead of awaited value.
         */
        error?: Error;
    }

    /**
     * Describes instantly available or promised value.
     */
    export type SimpleAwaitableSource<T> = T | Promise<T>;

    /**
     * Describes function to be invoked for delivering instantly available or
     * promised value optionally in context of a selected cache record.
     */
    export type CacheableAwaitableSource<T> = (cacheKey: string) => SimpleAwaitableSource<T>;

    /**
     * Describes ways of retrieving an awaitable value supported by usePromise
     * composable.
     */
    export type AwaitableSource<T> = SimpleAwaitableSource<T> | CacheableAwaitableSource<T>;

    /**
     * Creates reactive description of an awaitable value.
     *
     * @param source awaitable value or some function delivering it on invocation
     * @param cacheKey optional key of cache record to use in combination with a function as `source`
     * @param cacheNamespace optional namespace of provided cache record' key
     * @param cacheMaxAge optional number of seconds the selected cache record may be used before re-fetching value from `source`
     */
    export function usePromise<T>(source: AwaitableSource<T>, cacheKey?: string, cacheNamespace?: string, cacheMaxAge?: number): Ref<ReactiveAwaitable<T>>;

	/**
	 * Watches provided reactive source for changes invoking given detector
	 * function to decide whether source is considered "done" and returns a
	 * promise resolved by then.
	 *
	 * If provided callback is throwing, the returned promise is rejected.
	 *
	 * @param source reactive source to watch for change
	 * @param detectorFn callback invoked with source to decide if it is now considered "done"
	 * @returns promise resolved with source when callback has returned true on it or rejected if callback has thrown
	 */
	export function useReactiveAsPromise<T>(source: Ref<T>|ComputedRef<T>|UnwrapNestedRefs<T>, detectorFn: (source: T) => (boolean|Promise<boolean>)): Promise<Ref<T>|ComputedRef<T>|UnwrapNestedRefs<T>>;

	/**
	 * Sets up mutation observer watching <html> element of current document for
	 * class applied by VitePress to control color theme to use and adds
	 * according class of Shoelace to achieve the same for its components.
	 *
	 * It can be invoked multiple times without effect, every time returning the
	 * same callback to invoke for stopping that sole mutation set up at first
	 * invocation. When running the callback, observation is cancelled. Invoking
	 * this composable after that will set up another mutation observer just
	 * like before.
	 *
	 * @returns function to invoke for stopping observation
	 */
	export function useThemeSync(): () => void;
}
