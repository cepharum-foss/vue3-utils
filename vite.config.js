import { defineConfig } from "vite";
import { fileURLToPath } from "node:url";
import vue from "@vitejs/plugin-vue";

export default defineConfig( {
	plugins: [vue()],
	build: {
		lib: {
			entry: fileURLToPath( new URL( "./index.js", import.meta.url ) ),
			name: "cepharum-vue3-utils",
			fileName: "vue3-utils",
		},
		rollupOptions: {
			external: [
				"vue",
			],
			output: {
				globals: {
					vue: "Vue",
				}
			}
		}
	},
	test: {
		globals: true,
		environment: "jsdom",
	}
} );
