# @cepharum/vue3-utils

_a collection of common [Vue3](https://vuejs.org/) utilities_

## License

[MIT](LICENSE)

## Manual

https://cepharum-foss.gitlab.io/vue3-utils/
