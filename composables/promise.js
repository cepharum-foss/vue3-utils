import { isReactive, isRef, ref, unref, watchEffect } from "vue";
import { useAgingCache } from "./aging-cache.js";

/**
 * Creates reactive ref caching result of provided promise.
 *
 * @param {AwaitableSource} awaitable promise for some value, some value itself or a function to call for retrieving either
 * @param {string} cacheKey key identifying cache record to use in context of an optionally customized namespace
 * @param {string} cacheNamespace namespace of cache records to use
 * @param {number} cacheMaxAge custom maximum age of selected cache record in seconds
 * @returns {Ref<ReactiveAwaitable>} reactive object receiving promised result on resolving promise or the cause for its failure
 */
export function usePromise( awaitable, cacheKey = undefined, cacheNamespace = "", cacheMaxAge = undefined ) {
	const result = ref( null );

	try {
		let promise;

		if ( typeof awaitable === "function" ) {
			const cache = cacheKey == null ? undefined : useAgingCache( cacheKey, cacheNamespace, cacheMaxAge );

			if ( cache?.exists() ) {
				return cache.read();
			}

			promise = awaitable.call( this, cacheKey );

			cache?.write( result );
		} else {
			promise = awaitable;
		}

		if ( promise instanceof Promise ) {
			promise
				.then( record => {
					result.value = { record };
				} )
				.catch( error => {
					result.value = { error };
				} );
		} else {
			result.value = { record: promise };
		}
	} catch ( error ) {
		result.value = { error };
	}

	return result;
}

/**
 * Watches provided source for changes invoking given detector function on every
 * change to decide whether resulting promise is settled or not.
 *
 * @template T
 * @param {Ref<T>|ComputedRef<T>|UnwrapNestedRefs<T>} source reactive source to watch
 * @param {function(source: T):(boolean|Promise<boolean>)} detectorFn callback invoked to decide if source is "done", may throw to reject source
 * @returns {Promise<Ref<T>|ComputedRef<T>|UnwrapNestedRefs<T>>} promise resolved when detector callback returns truthy or rejected if detector callback throws
 */
export function useReactiveAsPromise( source, detectorFn ) {
	if ( !source || ( !isReactive( source ) && !isRef( source ) ) ) {
		return Promise.reject( new TypeError( "missing or invalid reactive source to watch" ) );
	}

	if ( typeof detectorFn !== "function" ) {
		return Promise.reject( new TypeError( "missing or invalid detector callback" ) );
	}

	let stopWatching;

	return new Promise( ( resolve, reject ) => {
		stopWatching = watchEffect( async() => {
			try {
				const isDone = await detectorFn( unref( source ) );

				if ( isDone ) {
					resolve( source );
				}
			} catch ( cause ) {
				reject( cause );
			}
		} );
	} )
		.finally( stopWatching );
}
