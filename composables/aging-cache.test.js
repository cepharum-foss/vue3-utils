import "should";
import { beforeEach, describe, it } from "vitest";
import { CacheRecord, useAgingCache } from "./aging-cache.js";

const wait = ( ms, value ) => new Promise( resolve => setTimeout( resolve, ms, value ) );

describe( "Composable method `useAgingCache()", () => {
	beforeEach( () => {
		CacheRecord.disableGC();
	} );

	it( "can be invoked without any argument", () => {
		( () => useAgingCache() ).should.not.throw();
	} );

	it( "retrieves a manager for a selected cache record", () => {
		const manager = useAgingCache();

		manager.should.have.property( "exists" ).which.is.a.Function();
		manager.should.have.property( "read" ).which.is.a.Function();
		manager.should.have.property( "write" ).which.is.a.Function();
		manager.should.have.property( "delete" ).which.is.a.Function();

		manager.constructor.should.have.property( "enableGC" ).which.is.a.Function();
		manager.constructor.should.have.property( "disableGC" ).which.is.a.Function();
	} );

	it( "retrieves separate managers for accessing same selected cache record on multiple invocations without any argument", () => {
		const a = useAgingCache();
		const b = useAgingCache();
		const c = useAgingCache();

		a.should.not.be.equal( b );
		a.should.not.be.equal( c );
		b.should.not.be.equal( c );

		a.exists().should.be.false();
		b.exists().should.be.false();
		c.exists().should.be.false();

		const value = Symbol();

		a.write( value );

		a.exists().should.be.true();
		b.exists().should.be.true();
		c.exists().should.be.true();

		b.read().should.equal( value );
		c.read().should.equal( value );

		c.delete();

		a.exists().should.be.false();
		b.exists().should.be.false();
		c.exists().should.be.false();
	} );

	it( "retrieves separate managers for accessing same selected cache record on multiple invocations with same cache key", () => {
		const a = useAgingCache( "cacheKey" );
		const b = useAgingCache( "cacheKey" );
		const c = useAgingCache( "cacheKey" );

		a.should.not.be.equal( b );
		a.should.not.be.equal( c );
		b.should.not.be.equal( c );

		a.exists().should.be.false();
		b.exists().should.be.false();
		c.exists().should.be.false();

		const value = Symbol();

		a.write( value );

		a.exists().should.be.true();
		b.exists().should.be.true();
		c.exists().should.be.true();

		b.read().should.equal( value );
		c.read().should.equal( value );

		c.delete();

		a.exists().should.be.false();
		b.exists().should.be.false();
		c.exists().should.be.false();
	} );

	it( "retrieves separate managers for accessing same selected cache record on multiple invocations with same cache key and cache key namespace", () => {
		const a = useAgingCache( "cacheKey", "cacheKeyNameSpaceFoo" );
		const b = useAgingCache( "cacheKey", "cacheKeyNameSpaceFoo" );
		const c = useAgingCache( "cacheKey", "cacheKeyNameSpaceFoo" );

		a.should.not.be.equal( b );
		a.should.not.be.equal( c );
		b.should.not.be.equal( c );

		a.exists().should.be.false();
		b.exists().should.be.false();
		c.exists().should.be.false();

		const value = Symbol();

		a.write( value );

		a.exists().should.be.true();
		b.exists().should.be.true();
		c.exists().should.be.true();

		b.read().should.equal( value );
		c.read().should.equal( value );

		c.delete();

		a.exists().should.be.false();
		b.exists().should.be.false();
		c.exists().should.be.false();
	} );

	it( "retrieves separate managers for accessing separate cache records on multiple invocations with different cache key", () => {
		const a = useAgingCache( "cacheKey1" );
		const b = useAgingCache( "cacheKey2" );
		const c = useAgingCache( "cacheKey3" );

		a.should.not.be.equal( b );
		a.should.not.be.equal( c );
		b.should.not.be.equal( c );

		a.exists().should.be.false();
		b.exists().should.be.false();
		c.exists().should.be.false();

		const value = Symbol();

		a.write( value );

		a.exists().should.be.true();
		b.exists().should.be.false();
		c.exists().should.be.false();

		a.read().should.equal( value );
		( b.read() == null ).should.be.true();
		( c.read() == null ).should.be.true();

		c.delete();

		a.exists().should.be.true();
		b.exists().should.be.false();
		c.exists().should.be.false();
	} );

	it( "retrieves separate managers for accessing separate cache records on multiple invocations with different cache key, but same cache key namespace", () => {
		const a = useAgingCache( "cacheKey1", "cacheKeyNameSpace" );
		const b = useAgingCache( "cacheKey2", "cacheKeyNameSpace" );
		const c = useAgingCache( "cacheKey3", "cacheKeyNameSpace" );

		a.should.not.be.equal( b );
		a.should.not.be.equal( c );
		b.should.not.be.equal( c );

		a.exists().should.be.false();
		b.exists().should.be.false();
		c.exists().should.be.false();

		const value = Symbol();

		a.write( value );

		a.exists().should.be.true();
		b.exists().should.be.false();
		c.exists().should.be.false();

		a.read().should.equal( value );
		( b.read() == null ).should.be.true();
		( c.read() == null ).should.be.true();

		c.delete();

		a.exists().should.be.true();
		b.exists().should.be.false();
		c.exists().should.be.false();
	} );

	it( "retrieves separate managers for accessing separate cache records on multiple invocations with same cache key, but different cache key namespace", () => {
		const a = useAgingCache( "cacheKey", "cacheKeyNameSpaceFoo" );
		const b = useAgingCache( "cacheKey", "cacheKeyNameSpaceBar" );
		const c = useAgingCache( "cacheKey", "cacheKeyNameSpaceBaz" );

		a.should.not.be.equal( b );
		a.should.not.be.equal( c );
		b.should.not.be.equal( c );

		a.exists().should.be.false();
		b.exists().should.be.false();
		c.exists().should.be.false();

		const value = Symbol();

		a.write( value );

		a.exists().should.be.true();
		b.exists().should.be.false();
		c.exists().should.be.false();

		a.read().should.equal( value );
		( b.read() == null ).should.be.true();
		( c.read() == null ).should.be.true();

		c.delete();

		a.exists().should.be.true();
		b.exists().should.be.false();
		c.exists().should.be.false();
	} );

	it( "auto-deletes an existing cache record when reaching certain age", async() => {
		const a = useAgingCache( "cacheKey", "cacheKeyNameSpace", 0.1 );
		const b = useAgingCache( "cacheKey", "cacheKeyNameSpace", 0.1 );

		a.should.not.be.equal( b );

		a.exists().should.be.false();
		b.exists().should.be.false();

		const value = Symbol();

		a.write( value );

		a.exists().should.be.true();
		b.exists().should.be.true();

		b.read().should.equal( value );

		await wait( 200 );

		a.exists().should.be.false();
		b.exists().should.be.false();
	} );

	it( "restarts a record's age counter when reading it by default", async() => {
		const a = useAgingCache( "cacheKey", "cacheKeyNameSpace", 0.1 );
		const b = useAgingCache( "cacheKey", "cacheKeyNameSpace", 0.1 );

		a.should.not.be.equal( b );

		a.exists().should.be.false();
		b.exists().should.be.false();

		const value = Symbol();

		a.write( value );

		a.exists().should.be.true();
		b.exists().should.be.true();

		b.read().should.equal( value );

		const handle = setInterval( () => b.read(), 50 );

		await wait( 200 );

		a.exists().should.be.true();
		b.exists().should.be.true();

		a.delete();
		clearInterval( handle );
	} );

	it( "does not restart a record's age counter when reading it on demand", async() => {
		const a = useAgingCache( "cacheKey", "cacheKeyNameSpace", 0.1 );
		const b = useAgingCache( "cacheKey", "cacheKeyNameSpace", 0.1 );

		a.should.not.be.equal( b );

		a.exists().should.be.false();
		b.exists().should.be.false();

		const value = Symbol();

		a.write( value );

		a.exists().should.be.true();
		b.exists().should.be.true();

		b.read().should.equal( value );

		const handle = setInterval( () => b.read( false ), 50 );

		await wait( 200 );

		a.exists().should.be.false();
		b.exists().should.be.false();

		clearInterval( handle );
	} );

	it( "supports garbage collection", async() => {
		const a = useAgingCache( "cacheKey1", "cacheKeyNameSpace", 0.1 );
		const b = useAgingCache( "cacheKey2", "cacheKeyNameSpace", 0.5 );

		CacheRecord.enableGC( 0.2 );

		a.write( 123 );
		b.write( 456 );

		a.exists().should.be.true();
		b.exists().should.be.true();

		await wait( 350 );

		a.exists().should.be.false();
		b.exists().should.be.true();

		await wait( 350 );

		a.exists().should.be.false();
		b.exists().should.be.false();
	} );

	it( "requires argument provided on enabling garbage collection to be a positive number of seconds", () => {
		( () => CacheRecord.enableGC( null ) ).should.throw();
		( () => CacheRecord.enableGC( true ) ).should.throw();
		( () => CacheRecord.enableGC( false ) ).should.throw();
		( () => CacheRecord.enableGC( "" ) ).should.throw();
		( () => CacheRecord.enableGC( "foo" ) ).should.throw();
		( () => CacheRecord.enableGC( "123foo" ) ).should.throw();
		( () => CacheRecord.enableGC( "-123" ) ).should.throw();
		( () => CacheRecord.enableGC( -123 ) ).should.throw();
		( () => CacheRecord.enableGC( -1.23 ) ).should.throw();
		( () => CacheRecord.enableGC( 0 ) ).should.throw();
		( () => CacheRecord.enableGC( [] ) ).should.throw();
		( () => CacheRecord.enableGC( {} ) ).should.throw();

		( () => CacheRecord.enableGC() ).should.not.throw(); // uses internal default
		( () => CacheRecord.enableGC( undefined ) ).should.not.throw(); // uses internal default

		( () => CacheRecord.enableGC( 123 ) ).should.not.throw();
		( () => CacheRecord.enableGC( "123" ) ).should.not.throw();
		( () => CacheRecord.enableGC( 1.23 ) ).should.not.throw();
		( () => CacheRecord.enableGC( "1.23" ) ).should.not.throw();
	} );
} );
