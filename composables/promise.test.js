import "should";
import { describe } from "vitest";
import { computed, isRef, reactive, ref } from "vue";
import { useReactiveAsPromise, usePromise } from "./promise.js";

const wait = ( ms, value ) => new Promise( resolve => setTimeout( resolve, ms, value ) );

describe( "Composable function `usePromise()`", () => {
	it( "can be invoked without any argument", () => {
		( () => usePromise() ).should.not.throw();
	} );

	it( "returns a ref() representing (missing) argument as result", () => {
		isRef( usePromise() ).should.be.true();
		usePromise().value.should.deepEqual( { record: undefined } );
	} );

	it( "returns a ref() representing instantly available argument as result", () => {
		usePromise( null ).value.should.deepEqual( { record: null } );
		usePromise( false ).value.should.deepEqual( { record: false } );
		usePromise( true ).value.should.deepEqual( { record: true } );
		usePromise( 1 ).value.should.deepEqual( { record: 1 } );
		usePromise( 0 ).value.should.deepEqual( { record: 0 } );
		usePromise( -456.987 ).value.should.deepEqual( { record: -456.987 } );
		usePromise( "" ).value.should.deepEqual( { record: "" } );
		usePromise( "hello world" ).value.should.deepEqual( { record: "hello world" } );
		usePromise( { name: "hello world" } ).value.should.deepEqual( { record: { name: "hello world" } } );
		usePromise( ["hello world"] ).value.should.deepEqual( { record: ["hello world"] } );
	} );

	it( "returns a ref() representing instantly available value delivered by function provided as argument as result", () => {
		usePromise( () => undefined ).value.should.deepEqual( { record: undefined } );
		usePromise( () => null ).value.should.deepEqual( { record: null } );
		usePromise( () => false ).value.should.deepEqual( { record: false } );
		usePromise( () => true ).value.should.deepEqual( { record: true } );
		usePromise( () => 1 ).value.should.deepEqual( { record: 1 } );
		usePromise( () => 0 ).value.should.deepEqual( { record: 0 } );
		usePromise( () => -456.987 ).value.should.deepEqual( { record: -456.987 } );
		usePromise( () => "" ).value.should.deepEqual( { record: "" } );
		usePromise( () => "hello world" ).value.should.deepEqual( { record: "hello world" } );
		usePromise( () => ( { name: "hello world" } ) ).value.should.deepEqual( { record: { name: "hello world" } } );
		usePromise( () => ["hello world"] ).value.should.deepEqual( { record: ["hello world"] } );
	} );

	it( "returns a ref() which is falsy initially when providing a promise to wait for", () => {
		const promise = wait( 50 );
		const record = usePromise( promise );

		isRef( record ).should.be.true();
		( record.value == null ).should.be.true();
	} );

	it( "returns a ref() which is falsy initially when providing a function returning a promise to wait for", () => {
		const promise = wait( 50 );
		const record = usePromise( () => promise );

		isRef( record ).should.be.true();
		( record.value == null ).should.be.true();
	} );

	it( "returns a ref() which is updated with result of provided promise as property `record` when that promise has resolved", async() => {
		const promise = wait( 50, 123 );
		const record = usePromise( promise );

		isRef( record ).should.be.true();
		( record.value == null ).should.be.true();

		await wait( 100 );

		isRef( record ).should.be.true();
		record.value.should.deepEqual( { record: 123 } );
	} );

	it( "returns a ref() which is updated with result of promise returned from a provided function as property `record` when that promise has resolved", async() => {
		const promise = wait( 50, 123 );
		const record = usePromise( () => promise );

		isRef( record ).should.be.true();
		( record.value == null ).should.be.true();

		await wait( 100 );

		isRef( record ).should.be.true();
		record.value.should.deepEqual( { record: 123 } );
	} );

	it( "returns a ref() which is updated with reason of provided promise being rejected as property `error`", async() => {
		const promise = wait( 50 ).then( () => {
			throw new Error( "this is an error" );
		} );
		const record = usePromise( promise );

		isRef( record ).should.be.true();
		( record.value == null ).should.be.true();

		await wait( 100 );

		isRef( record ).should.be.true();
		record.value.should.deepEqual( { error: new Error( "this is an error" ) } );
	} );

	it( "returns a ref() which is updated with reason of rejected promise returned from provided function as property `error`", async() => {
		const promise = wait( 50 ).then( () => {
			throw new Error( "this is an error" );
		} );
		const record = usePromise( () => promise );

		isRef( record ).should.be.true();
		( record.value == null ).should.be.true();

		await wait( 100 );

		isRef( record ).should.be.true();
		record.value.should.deepEqual( { error: new Error( "this is an error" ) } );
	} );

	it( "returns a ref() instantly holding reason of provided function throwing in property `error`", () => {
		const record = usePromise( () => {
			throw new Error( "this is an error" );
		} );

		isRef( record ).should.be.true();
		record.value.should.deepEqual( { error: new Error( "this is an error" ) } );
	} );

	it( "returns different ref() on multiple invocations with function for generating a result using no cache key on every invocation", () => {
		const fn = () => 123;
		const a = usePromise( fn );
		const b = usePromise( fn );
		const c = usePromise( () => 123 );

		a.should.not.be.equal( b );
		a.should.not.be.equal( c );
		b.should.not.be.equal( c );
	} );

	it( "returns different ref() on multiple invocations with function for generating a result using different cache key on every invocation", () => {
		const fn = () => 123;
		const a = usePromise( fn, "uniqueCacheId1" );
		const b = usePromise( fn, "uniqueCacheId2" );
		const c = usePromise( () => 123, "uniqueCacheId3" );

		a.should.not.be.equal( b );
		a.should.not.be.equal( c );
		b.should.not.be.equal( c );
	} );

	it( "returns same ref() on multiple invocations with a function for generating a result using same cache key on every invocation", () => {
		const fn = () => 123;
		const a = usePromise( fn, "uniqueCacheId" );
		const b = usePromise( fn, "uniqueCacheId" );
		const c = usePromise( () => 123, "uniqueCacheId" );

		a.should.be.equal( b );
		a.should.be.equal( c );
	} );
} );

describe( "Composable function `useReactiveAsPromise()`", () => {
	it( "rejects returned promise when invoked without any argument", async() => {
		await useReactiveAsPromise().should.be.rejected();
	} );

	it( "rejects returned promise when invoked without a non-reactive argument", async() => {
		await useReactiveAsPromise( undefined ).should.be.rejected();
		await useReactiveAsPromise( null ).should.be.rejected();
		await useReactiveAsPromise( false ).should.be.rejected();
		await useReactiveAsPromise( true ).should.be.rejected();
		await useReactiveAsPromise( 1 ).should.be.rejected();
		await useReactiveAsPromise( -234.56 ).should.be.rejected();
		await useReactiveAsPromise( "-234.56" ).should.be.rejected();
		await useReactiveAsPromise( "hello" ).should.be.rejected();
		await useReactiveAsPromise( () => ref( true ) ).should.be.rejected();
		await useReactiveAsPromise( {} ).should.be.rejected();
		await useReactiveAsPromise( { hello: "foo" } ).should.be.rejected();
		await useReactiveAsPromise( [] ).should.be.rejected();
		await useReactiveAsPromise( [ "hello", 123 ] ).should.be.rejected();
	} );

	it( "rejects returned promise when invoked without a detector callback", async() => {
		await useReactiveAsPromise( ref(), undefined ).should.be.rejected();
		await useReactiveAsPromise( ref(), null ).should.be.rejected();
		await useReactiveAsPromise( ref(), false ).should.be.rejected();
		await useReactiveAsPromise( ref(), true ).should.be.rejected();
		await useReactiveAsPromise( ref(), 1 ).should.be.rejected();
		await useReactiveAsPromise( ref(), -234.56 ).should.be.rejected();
		await useReactiveAsPromise( ref(), "-234.56" ).should.be.rejected();
		await useReactiveAsPromise( ref(), "hello" ).should.be.rejected();
		await useReactiveAsPromise( ref(), {} ).should.be.rejected();
		await useReactiveAsPromise( ref(), { hello: "foo" } ).should.be.rejected();
		await useReactiveAsPromise( ref(), [] ).should.be.rejected();
		await useReactiveAsPromise( ref(), [ "hello", 123 ] ).should.be.rejected();
	} );

	it( "resolves returned promise when invoked with a ref() and a detector callback which is returning truthy", async() => {
		await useReactiveAsPromise( ref(), () => true ).should.not.be.rejected();
	} );

	it( "resolves returned promise when invoked with a reactive() and a detector callback which is returning truthy", async() => {
		await useReactiveAsPromise( reactive( {} ), () => true ).should.not.be.rejected();
	} );

	it( "resolves returned promise when invoked with a computed() and a detector callback which is returning truthy", async() => {
		await useReactiveAsPromise( computed( () => ( {} ) ), () => true ).should.not.be.rejected();
	} );

	it( "keeps pending returned promise when invoked with a ref() and a detector callback which is returning falsy", async() => {
		await Promise.race( [
			useReactiveAsPromise( ref(), () => false ),
			wait( 50, 123 ),
		] ).should.be.resolvedWith( 123 );
	} );

	it( "keeps pending returned promise when invoked with a reactive() and a detector callback which is returning falsy", async() => {
		await Promise.race( [
			useReactiveAsPromise( reactive( {} ), () => false ),
			wait( 50, 123 ),
		] ).should.be.resolvedWith( 123 );
	} );

	it( "keeps pending returned promise when invoked with a computed() and a detector callback which is returning falsy", async() => {
		await Promise.race( [
			useReactiveAsPromise( computed( () => ( {} ) ), () => false ),
			wait( 50, 123 ),
		] ).should.be.resolvedWith( 123 );
	} );

	it( "resolves returned promise when invoked with a ref() and a detector callback which is returning truthy after change of ref()", async() => {
		const source = ref();

		setTimeout( () => {
			source.value = 1;
		}, 50 );

		const result = await Promise.race( [
			useReactiveAsPromise( source, value => Boolean( value ) ),
			wait( 100, 123 ),
		] );

		result.should.equal( source );
		result.should.have.property( "value" ).which.is.equal( 1 );
	} );

	it( "resolves returned promise when invoked with a reactive() and a detector callback which is returning truthy after change of reactive()", async() => {
		const source = reactive( {} );

		setTimeout( () => {
			source.prop = 1;
		}, 50 );

		const result = await Promise.race( [
			useReactiveAsPromise( source, record => Boolean( record.prop ) ),
			wait( 100, 123 ),
		] );

		result.should.equal( source );
		result.should.have.property( "prop" ).which.is.equal( 1 );
	} );

	it( "resolves returned promise when invoked with a computed() and a detector callback which is returning truthy after change of computed()", async() => {
		const source = ref();
		const copy = computed( () => source.value > 0 );

		setTimeout( () => {
			source.value = 1;
		}, 50 );

		const result = await Promise.race( [
			useReactiveAsPromise( copy, value => Boolean( value ) ),
			wait( 100, 123 ),
		] );

		result.should.equal( copy );
	} );

	it( "rejects returned promise when invoked with a ref() and a detector callback which is throwing after change of ref()", async() => {
		const source = ref();

		setTimeout( () => {
			source.value = 1;
		}, 50 );

		await Promise.race( [
			useReactiveAsPromise( source, record => {
				if ( record ) {
					throw new Error( "detection failed" );
				}

				return false;
			} ),
			wait( 100, 123 ),
		] )
			.should.be.rejectedWith( new Error( "detection failed" ) );
	} );

	it( "rejects returned promise when invoked with a reactive() and a detector callback which is throwing after change of reactive()", async() => {
		const source = reactive( {} );

		setTimeout( () => {
			source.prop = 1;
		}, 50 );

		await Promise.race( [
			useReactiveAsPromise( source, record => {
				if ( record.prop ) {
					throw new Error( "detection failed" );
				}

				return false;
			} ),
			wait( 100, 123 ),
		] )
			.should.be.rejectedWith( new Error( "detection failed" ) );
	} );

	it( "rejects returned promise when invoked with a computed() and a detector callback which is throwing after change of computed()", async() => {
		const source = ref();
		const copy = computed( () => source.value > 0 );

		setTimeout( () => {
			source.value = 1;
		}, 50 );

		await Promise.race( [
			useReactiveAsPromise( copy, record => {
				if ( record ) {
					throw new Error( "detection failed" );
				}

				return false;
			} ),
			wait( 100, 123 ),
		] )
			.should.be.rejectedWith( new Error( "detection failed" ) );
	} );
} );
