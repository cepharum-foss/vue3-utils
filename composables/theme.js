let stopThemeSync;

/**
 * Sets up mutation observer to keep Shoelace theme in sync with VitePress
 * theme. Use this in projects based on VitePress when using Shoelace
 * components.
 *
 * @returns {stopThemeSync} callback to invoke for stopping synchronization
 */
export function useThemeSync() {
	if ( !stopThemeSync ) {
		if ( typeof document !== "undefined" && typeof MutationObserver !== "undefined" ) {
			const element = document.documentElement;

			const sync = () => {
				if ( element.classList.contains( "dark" ) ) {
					element.classList.remove( "sl-theme-light" );
					element.classList.add( "sl-theme-dark" );
				} else {
					element.classList.remove( "sl-theme-dark" );
					element.classList.add( "sl-theme-light" );
				}
			};

			const observer = new MutationObserver( () => {
				if ( element.classList.contains( "dark" ) !== element.classList.contains( "sl-theme-dark" ) ) {
					setTimeout( sync );
				}
			} );

			observer.observe( element, { attributes: true, attributeFilter: ["class"] } );

			sync();

			stopThemeSync = () => {
				observer.disconnect();
				stopThemeSync = undefined;
			};
		} else {
			stopThemeSync = () => {
				stopThemeSync = undefined;
			};
		}
	}

	return stopThemeSync;
}
