const Cache = new Map();

const MaxAge = 10;

/**
 * Implements manager for a single pre-selected record in cache.
 */
export class CacheRecord {
	/**
	 * Provides record's full cache key.
	 */
	#cacheKey;

	/**
	 * Provides number of milliseconds to wait before removing an outdated
	 * record from cache again.
	 */
	#maxAge = MaxAge * 1000;

	/**
	 * Provides number of milliseconds to wait before running garbage collection
	 * again.
	 *
	 * @type {number}
	 */
	static #gcCycle = 10000;

	/**
	 * Tracks handle of interval timer re-running garbage collection.
	 *
	 * @type {unknown}
	 */
	static #gcTimer;

	/**
	 * @param {string} fullKey unique key of cache record to manage
	 * @param {number} maxAge custom maximum age of managed record in seconds
	 */
	constructor( fullKey, maxAge = undefined ) {
		this.#cacheKey = fullKey;

		const _maxAge = Number( maxAge );
		if ( _maxAge > 0 ) {
			this.#maxAge = _maxAge * 1000;
		}
	}

	/**
	 * Detects if pre-selected cache record exists and hasn't exceeded its
	 * maximum age.
	 *
	 * @returns {boolean} true if cache record exists and is still valid, false otherwise
	 */
	exists() {
		let hasRecord = Cache.has( this.#cacheKey );

		if ( hasRecord ) {
			const [ timestamp, maxAge ] = Cache.get( this.#cacheKey );

			if ( Date.now() - timestamp > maxAge ) {
				Cache.delete( this.#cacheKey );

				hasRecord = false;
			}
		}

		return hasRecord;
	}

	/**
	 * Retrieves pre-selected record from cache.
	 *
	 * @param {boolean} refresh if true [default], reading the record will restart its age counter
	 * @returns {undefined|*} found cache record or undefined if it is missing or has exceeded its maximum age
	 */
	read( refresh = true ) {
		if ( this.exists() ) {
			const record = Cache.get( this.#cacheKey );

			if ( refresh ) {
				record[0] = Date.now();
			}

			return record[2];
		}

		return undefined;
	}

	/**
	 * Writes provided value into pre-selected cache record.
	 *
	 * @param {*} value value to be written into cache record
	 * @returns {CacheRecord} current cache record manager
	 */
	write( value ) {
		Cache.set( this.#cacheKey, [ Date.now(), this.#maxAge, value ] );

		if ( !this.constructor.#gcTimer && this.constructor.#gcCycle > 0 ) {
			this.constructor.#gcTimer = setInterval( () => this.constructor.gc(), this.constructor.#gcCycle );
		}

		return this;
	}

	/**
	 * Removes pre-selected record from cache.
	 *
	 * @returns {CacheRecord} current cache record manager
	 */
	delete() {
		Cache.delete( this.#cacheKey );

		return this;
	}

	/**
	 * Garbage-collects all records of cache that have exceeded this (!!)
	 * cache record manager's maximum age.
	 *
	 * @returns {{before: number, after: number}} number of records in cache before and after garbage-collecting them
	 */
	static gc() {
		const now = Date.now();
		const before = Cache.size;

		for ( const [ k, [ timestamp, maxAge ] ] of Cache.entries() ) {
			if ( now - timestamp >= maxAge ) {
				Cache.delete( k );
			}
		}

		const after = Cache.size;

		if ( !after ) {
			clearInterval( this.#gcTimer );
			this.#gcTimer = undefined;
		}

		return { before, after };
	}

	/**
	 * Enables garbage collection every given number of seconds until whenever
	 * cache has records.
	 *
	 * @param {number} cycle number of seconds after which garbage collection has to be re-run
	 * @returns {void}
	 */
	static enableGC( cycle = 10 ) {
		const seconds = Number( String( cycle ) );
		if ( !( seconds > 0 ) ) {
			throw new TypeError( "missing or invalid number of seconds per garbage collection cycle" );
		}

		if ( this.#gcTimer ) {
			clearInterval( this.#gcTimer );
		}

		this.#gcCycle = seconds * 1000;

		if ( Cache.size > 0 ) {
			this.#gcTimer = setInterval( () => this.gc(), this.#gcCycle );
		}
	}

	/**
	 * Disables garbage collection.
	 *
	 * @returns {void}
	 */
	static disableGC() {
		if ( this.#gcTimer ) {
			clearInterval( this.#gcTimer );
		}

		this.#gcCycle = 0;
	}
}

/**
 * Provides access on a cache record.
 *
 * @param {string} key local key of cache record
 * @param {string} namespace namespace of key
 * @param {number} maxAge number of seconds this record may survive in cache
 * @returns {CacheRecord} accessor for selected cache record
 */
export function useAgingCache( key, namespace = "", maxAge = undefined ) {
	return new CacheRecord( namespace + ":" + key, maxAge );
}
