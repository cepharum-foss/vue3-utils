---
outline:
  level: [2, 3]
---

# Themes

## useThemeSync()

This composable is useful in projects based on [VitePress](https://vitepress.dev/) and its default theme while using custom components based on those provided by [Shoelace](https://shoelace.style/).

When invoked, the composable sets up a mutation observer for the `class` attribute of current document's `<html>` element. It detects any class applied there by VitePress for controlling whether to show light color theme or dark color theme and adds according class of Shoelace components.

```javascript
import { useThemeSync } from "@cepharum/vue3-utils";

const stopFn = useThemeSync();
```

The composable returns a function that can be invoked to shut down the mutation observer and hence the syncing of color themes.

### Multiple use

A single mutation observer is set up on first invocation. On consuming this composable multiple times, no additional observer is set up but the same function for stopping that original observer is returned. Another mutation observer is set up when invoking this composable next time after using the returned function to stop the observation.

### Best practice

The syncing should be set up as part of [extending the default theme of VitePress](https://vitepress.dev/guide/extending-default-theme):

::: code-group
```javascript [.vitepress/theme/index.js]
import DefaultTheme from "vitepress/theme";
import { useThemeSync } from "@cepharum/vue3-utils"; // [!code ++]

/** @type {import('vitepress').Theme} */
export default {
  extends: DefaultTheme,
  enhanceApp({ app }) {
    useThemeSync(); // [!code ++]
  }
}
```
:::
