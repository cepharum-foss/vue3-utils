---
outline:
  level: [2,3]
---

# Aging Cache

The composable aging cache provides a common way of caching data short term in local runtime. It manages key-based cache records and considers them outdated after a configurable amount of time.

```javascript
import { useAgingCache } from "@cepharum/vue3-utils";

const record = useAgingCache( 'unique-id-of-cached-record' );

if ( record.exists() ) {
    myCustomProcess( record.read() );
} else {
    const value = myCustomGenerator();
    
    record.write( value );

    myCustomProcess( value );
}
```

This example illustrates most basic use case of preferring an existing cache record on a value over re-generating that value and tracking it in cache for re-use.

## useAgingCache()

The composable function `useAgingCache()` generates an individual API for interacting with a pre-selected record of local cache. Supported parameters are

* a mandatory key uniquely selecting a record of cache in a namespace of cache records,
* an optional namespace of cache records and
* a custom time in seconds after which the requested cache record is considered obsolete.

It returns a simple API for accessing the particularly selected cache record.

## Cache record API

:::warning Possible race conditions  
Use any combination of these methods in synchronous code, only. Otherwise, garbage collection may have interfered with your code's assessment of current caching state. 
:::

### exists()

This method tests if the pre-selected cache record exists and is still valid.

### read()

This method reads an existing value from pre-selected cache record. It returns `undefined` if record does not exist or has been removed for being outdated. 

#### Record refreshing

By default, reading a cache record is resetting its time to live. Thus, frequently reading a cached record prevents it from ever becoming outdated implicitly. Pass `false` as argument to prevent this refreshing of a cache record on reading it:

```javascript
const value = record.read( false );
```

### write( value )

The method creates or updates pre-selected cache record to provide given value and resets its time to live.

### delete()

Use this method to explicitly remove the pre-selected record from cache.

### CacheRecord.gc()

This _static_ method can be used to explicitly trigger garbage collection to remove all outdated records from cache. By default, this method is set up to be invoked every few seconds unless the cache is empty.

```javascript
import { CacheRecord } from "@cepharum/vue3-utils";

CacheRecord.gc();
```

### CacheRecord.enableGC()

This _static_ method sets up frequent removal of outdated cache records every number of seconds given as optional first argument. When omitting the argument, an internal default is applied.

By default, garbage collection is enabled. You can use this function to re-enable garbage collection after having it disabled or to adjust the number of seconds after which another run of garbage collection should be performed. In the latter case, some running interval is cancelled and restarted.

```javascript
import { CacheRecord } from "@cepharum/vue3-utils";

CacheRecord.enableGC( 20 );
```

### CacheRecord.disableGC()

This _static_ method disables any automatic removal of outdated cache records by means of garbage collection. Any running interval is cancelled instantly.

```javascript
import { CacheRecord } from "@cepharum/vue3-utils";

CacheRecord.disableGC();
```
