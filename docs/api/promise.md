---
outline:
  level: [2, 3]
---

# Promises

The library provides two composable functions for conveniently dealing with promises in context of reactive code.

## usePromise()

This composable function is a helper to create a reactive record from a promise. 

```javascript
import { usePromise } from "@cepharum/vue3-utils";

const data = usePromise( fetchData() );
```

This example presents simple use case for converting data promised by a function (here `fetchData()`) into a [ref](https://vuejs.org/api/reactivity-core.html#ref) object updated once the promise has settled on success or on failure. 

The returned ref's initial value can be nullish. When a provided promise is resolved, the returned ref's value becomes an object with property `record` exposing promised value. If promise has been rejected, an object with property `error` is provided instead.

`usePromise()` supports promises as well as regular data. In the latter case, the returned ref is _instantly_ providing object with property `record` exposing that regular data.

```javascript
import { usePromise } from "@cepharum/vue3-utils";

const data = usePromise( () => {
    // TODO calculate the result, e.g.
    
    return "some data";
} );

console.log( data.value.record );     // --> "some data"
```

Eventually, you can provide a function invoked to generate either a promise or some regular data to be handled in the same way as described above:

```javascript
import { usePromise } from "@cepharum/vue3-utils";

const data = usePromise( fetchData );
```

In opposition to first example, the function `fetchData` isn't invoked first to pass its result as argument to `usePromise()`. Instead, the function is invoked by `usePromise()` internally to get that value. This difference is essential when enabling caching:

```javascript
import { usePromise } from "@cepharum/vue3-utils";

const data = usePromise( fetchData, "uniqueIdOfFetchedData" );
```

This time, `fetchData` is invoked to _synchronously_ produce data or a promise for data **unless** there is a valid cache record with ID `"uniqueIdOfFetchedData"` e.g. due to having run this code shortly before. Caching works in combination with providing a function as first argument to `usePromise()`, only. 

All caching is based on [`useAgingCache()`](aging-cache.md#useagingcache) internally. Because of that, a namespace and a custom maximum age for the cache record can be given in addition to the record's key:

```javascript
import { usePromise } from "@cepharum/vue3-utils";

const data = usePromise( fetchData, "uniqueIdOfFetchedData", "images", 60 );
```

The callback is invoked with provided key of cache record as first argument encouraging to use the same identifier for fetching and caching data:

```javascript
import { usePromise } from "@cepharum/vue3-utils";

const data = usePromise( id => fetchData( id ), "recordId" );
```

Providing a function is beneficial for dealing with exceptions thrown on invoking that function, too, resulting in returned ref representing that exception in the same way as the result of a rejected promise:

```javascript
import { usePromise } from "@cepharum/vue3-utils";

const data = usePromise( () => {
    // TODO implement the function
    
    throw new Error( "something happened" );
}, "recordId" );

console.log( data.value.error.message );    // --> "something happened"
```

## useReactiveAsPromise()

This composable function returns a promise which is resolved or rejected based on a watched reactive source.

```javascript
import { useReactiveAsPromise } from "@cepharum/vue3-utils";

const reactiveData = ref();

setTimeout( () => {
    reactiveData.value = "data arrived";
}, 1000 );

const result = await useReactiveAsPromise( reactiveData, data => data != null );

console.log( result === reactiveData );  // logs "true"
```

`useReactiveAsPromise()` supports a [ref](https://vuejs.org/api/reactivity-core.html#ref), a [computed](https://vuejs.org/api/reactivity-core.html#computed) or a [reactive](https://vuejs.org/api/reactivity-core.html#reactive) as its source. An additionally provided callback function is invoked every time the given source is changing. It is invoked with unwrapped source as argument. The callback is expected to return truthy on source being considered _ready_ or _done_ which is instantly causing the returned promise to be resolved with the source. On returning falsy, the source is kept watching to wait for another change to be re-assessed again. If the callback is throwing, the returned promise gets rejected with the thrown exception. `useReactiveAsPromise()` stops watching the source once the promise has been settled either way. 
