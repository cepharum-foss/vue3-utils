export default {
    title: "@cepharum/vue3-utils",
    description: "a collection of Vue3 utilities",
    base: "/vue3-utils",
    outDir: "../public",
    themeConfig: {
        sidebar: [
            {
                text: "Home",
                link: "/index.html",
            },
            {
                text: "Composables", items: [
                    {
                        text: "Aging cache",
                        link: "/api/aging-cache.html",
                    },
                    {
                        text: "Promises",
                        link: "/api/promise.html",
                    },
                    {
                        text: "Themes",
                        link: "/api/theme.html",
                    },
                ]
            },
        ]
    }
}
