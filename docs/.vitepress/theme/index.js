import DefaultTheme from "vitepress/theme";

import "./style.scss";

import ThisYear from "./components/ThisYear.vue";
import { useThemeSync } from "../../../composables/theme.js";

export default {
	extends: DefaultTheme,
	enhanceApp( ctx ) {
		useThemeSync();

		ctx.app.component( "ThisYear", ThisYear );
	},
};
